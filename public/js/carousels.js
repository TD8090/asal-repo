// Dropdown Menu Fade    
$(document).ready(function(){



    function setNavi( $c, $i ) {
        var title = $i.attr( 'alt' );
        $('#title').text( title );
    }


    $(function() {
        $("#topic-carousel").carouFredSel({
            responsive:true,
            width: 950,
            height: 290,
            align: "left",
            items: 1,
            prev: '#tcar-prev',
            next: '#tcar-next',
            pagination: {
                container: '#tcar-pager span',
                items: 1,
                anchorBuilder: function (nr, item) {
                    var index = nr - 1;
                    return '<a href=\'#\' data-slide-index=\'' + index + '\'><span>' + nr + '</span></a>';
                }
            },
            scroll: {
                onBefore: function( data ) {
                    setNavi( $(this), data.items.visible );
                },
                fx: "crossfade",
                timeoutDuration: 4000,
                duration: 800,
                items: 1
            },
            onCreate: function( data ) {
                setNavi( $(this), data.items );
            }
        });
    });
    $(function() {
        $("#features-carousel").carouFredSel({
            width: '100%',
            height: 180,
            items: 4,
            prev: '#fcar-prev',
            next: '#fcar-next',
            pagination: {
                container: '#fcar-pager span',
                items: 1,
                anchorBuilder: function (nr, item) {
                    var index = nr - 1;
                    return '<a href=\'#\' data-slide-index=\'' + index + '\'><span>' + nr + '</span></a>';
                }
            },
            scroll: {
                onBefore: function( data ) {
                    setNavi( $(this), data.items.visible );
                },
                fx: "linear",
                timeoutDuration: 4000,
                duration: 500,
                items:1
            },
            onCreate: function( data ) {
                setNavi( $(this), data.items );
            }
        });
    });





});




